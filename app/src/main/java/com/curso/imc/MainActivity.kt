package com.curso.imc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var resultado = 0.0
        var estaturaAlCuadrado = 0.0
        button.setOnClickListener {
            estaturaAlCuadrado = Math.pow(edt_estatura?.text.toString().toDouble(), 2.0)
            resultado = edt_peso.text.toString().toDouble() / estaturaAlCuadrado
            resultado = resultado.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
            if (resultado < 25) {
                tv_resultado.text =
                    "Ud se encuentra en Peso Normal su indice es: " + resultado.toString()
            } else if (resultado > 25 && resultado < 30) {
                tv_resultado.text =
                    "Ud se encuentra en Sobrepeso su indice es: " + resultado.toString()
            } else if (resultado > 30) {
                tv_resultado.text =
                    "Ud se encuentra en obesidad su indice es: " + resultado.toString()
            }
        }

    }
}
